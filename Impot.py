#T.P Python - conditions
#1/ Écrivez un programme qui résout le problème 4.5 du support algorithmique 0 - Algorithimique.pdf
#2/ Faites en sortes que l'on puisse tester plusieurs individus sans relancer le programme .
#3/ Vous testerez votre programme avec le jeu de test qui sera fourni

"""
while True:
    Hab = input("Habitez-vous à Zorglub (0 = vrai, Entrez = sortie) ? : ")
    if Hab == '':
        break
    Age = int(input("Entrez votre âge : "))
    Genre = input("Entrez votre genre ('F' ou 'M'): ")
    
    if Age > 20 and Genre == 'M':
        print("Monsieur, vous êtes imposable")
    elif Age <= 20 and Genre == 'M':
        print("Monsieur, vous n'êtes pas imposable")
    elif Age > 18 and Age < 35 and Genre == 'F':
        print("Madame, vous êtes imposable")
    else:
        print("Madame, vous n'êtes pas imposable")

print("Fin du programme")
"""






while True:
    sexe = input(
        "Entrez le code sexe de la personne (Mm:Male/F:Ffemale) (CR=Sortie) ")
    if sexe == '':
        break

    if not (sexe.lower() == 'f' or sexe.lower() == 'm'):
        print("Le code sexe n'est pas correct, entrez MmFf")
        continue

    age = input("Entrez l'âge : ")
    try:
        age = int(age)
    except ValueError:
        print(f"Entrez une valeur numérique : {age} ")
        continue

    C1 = (sexe.lower() == 'm' and age > 20)
    C2 = (sexe.lower() == 'f' and (age > 18 and age < 35))

    if C1 or C2:
        print("Zorglubien imposable")
    else:
        print("Zorglubien NON imposable")

print("fin du programme...")
