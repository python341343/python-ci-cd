"""T.P Python - calculs
1/ Écrivez un programme permettant de résoudre le problème énoncé au 4.4 de 0 - Algorithmique.pdf (reprographie)
2/ Votre programme doit permettre de calculer plusieurs factures sans relancer le programme
3/ A la fin du calcul vous afficherez le total général calculé
"""
fact = 0
while True:
    N = input("Entrez le nombre de copies souhaitez: ")
    p = 0
    if N == '':
        break

    try:
        N = int(N)
    except ValueError:
        print(f"Entez le nombre ex:act")
        continue

    if N <= 10:
        p = N * 0.1
        fact = fact + p
        print (f"le prix total est : {p}")
    elif N <= 30:
        p = 10*0.1 + (N-30) * 0.08
        fact = fact + p
        print (f"le prix total est : {p}")
        
print(f"Total facture: {fact}")
print("fin du programme...")
