# Créez un dictionnaire pour stocker les dépenses quotidiennes
expenses = {}

# Fonction pour ajouter des dépenses
def ajouter_depense(jour, montant):
    if jour in expenses:
        expenses[jour] += montant
    else:
        expenses[jour] = montant

# Fonction pour afficher les dépenses
def afficher_depenses():
    total = 0
    for jour, montant in expenses.items():
        total += montant
        print(f"{jour}: {montant} €")
    print(f"Total des dépenses: {total} €")

# Boucle pour l'interface utilisateur
while True:
    print("\nOptions:")
    print("1. Ajouter une dépense")
    print("2. Afficher les dépenses")
    print("3. Quitter")
    
    choix = input("Sélectionnez une option : ")
    
    if choix == '1':
        jour = input("Entrez la date (JJ/MM/AAAA) : ")
        montant = float(input("Entrez le montant de la dépense : "))
        ajouter_depense(jour, montant)
        print("Dépense ajoutée avec succès.")
    elif choix == '2':
        afficher_depenses()
    elif choix == '3':
        break
    else:
        print("Option non valide. Réessayez.")
