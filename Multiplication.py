#T.P Python - multiplication
#1/ Écrivez un programme python permettant à l'utilisateur de calculer des tables de multiplications
#2/ Le programme demandera à l'utilisateur quelle table il veut calculer 
#3/ Le programme affichera la table de multiplication de 1 à 10 du nombre entré par l'utilisateur



while True:
    rep = input("Entrez un nombre (Enter=Sortie) ? : ")
    if rep == '':
        break
    try:
        rep = int(rep)
    except ValueError:
        print(f"Entrez une valeur numÃ©rique : {rep} ")
        continue

    for idx in range(1, 11):
        print(f'{rep} X {idx} = {idx*int(rep)}')

print("Fin du programme de multiplication")

